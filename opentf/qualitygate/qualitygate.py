# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A qualitygate service."""

from typing import Any, Dict, List, Optional, Set, Tuple

import os

from urllib.parse import urlparse

from flask import request, Response

import requests
import yaml

from opentf.commons import (
    make_app,
    run_app,
    get_context_service,
    make_status_response,
    annotate_response,
    make_uuid,
    is_uuid,
    authorizer,
    can_use_namespace,
    validate_schema,
    SERVICECONFIG,
    QUALITY_GATE,
)

from opentf.commons.expressions import evaluate_bool
from opentf.toolkit import watch_file


########################################################################
# Constants

# status produced by the quality gate in details.status attribute :
# SUCCESS: the workflow has succeeded and quality gate strategy is green
# NOTEST: the workflow has succeeded, but it produced no test result.
# FAILURE: the workflow has failed, or the quality gate strategy is red
# RUNNING: the workflow execution is still ongoing.
# INVALID_SCOPE: some quality gate rule scopes are invalid.

SUCCESS = 'SUCCESS'
FAILURE = 'FAILURE'
RUNNING = 'RUNNING'
NOTEST = 'NOTEST'
INVALID_SCOPE = 'INVALID_SCOPE'


########################################################################
# Default quality gate configurations

DEFAULT_FAILURE_STATUS = ('failure', 'error', 'blocked')

DEFAULT_STRICT_QUALITY_GATE = [
    {
        'name': 'Default strict quality gate',
        'rule': {
            'scope': 'true',
            'threshold': '100%',
            'failure-status': DEFAULT_FAILURE_STATUS,
        },
    }
]

DEFAULT_PASSING_QUALITY_GATE = [
    {
        'name': 'Default passing quality gate',
        'rule': {
            'scope': 'true',
            'threshold': '0%',
            'failure-status': DEFAULT_FAILURE_STATUS,
        },
    }
]

########################################################################
# Helpers

# pylint: disable=no-member


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


def _get_namespace(testcases) -> str:
    for testcase in testcases:
        if namespace := testcase.get('metadata', {}).get('namespace'):
            return namespace
    raise ValueError('Missing metadata in testcases.')


def _check_duplicates(items: List[str], name: str) -> Optional[List[str]]:
    """Check for duplicate items."""
    if duplicates := [item for item in set(items) if items.count(item) >= 2]:
        msg = f'Duplicate {name} name(s) found: {", ".join(duplicates)}. The last definition(s) will be used.'
        return [msg]
    return None


def in_scope(expr: str, contexts: Dict[str, Any], scopes_errors: Set[str]) -> bool:
    """Safely evaluate datasource scope."""
    try:
        return evaluate_bool(expr, contexts)
    except ValueError as err:
        msg = f'Invalid conditional {expr}: {err}.'
        scopes_errors.add(msg)
    except KeyError as err:
        msg = f'Nonexisting context entry in expression {expr}: {err}.'
        scopes_errors.add(msg)
    return False


########################################################################
# Strategies


def evaluate_rule(
    rule: Dict[str, Any], testcases: Dict[str, Any], mode: str
) -> Dict[str, Any]:
    """Evaluate testcase results in rule scope.

    # Returned value

    A dictionary.  It contains at least a `result` entry, which is one
    of `NOTEST`, `SUCCESS`, or `FAILURE`.

    If `result` is not `NOTEST`, it also includes the following entries:

    - scope: a string
    - test_in_scope: an integer
    - test_failed: an integer
    - test_passed; an integer
    - success_ratio: a string
    """

    def _pass(threshold: str) -> bool:
        try:
            expected = float(threshold.rstrip('%')) / 100
            return (passed_count / testcases_count) >= expected
        except ValueError as err:
            debug(f'Invalid threshold value {threshold} for qualitygate {mode}: {err}')
        raise ValueError(f'Qualitygate threshold {threshold} is invalid.')

    scopes_errors = set()
    scope = rule['scope']
    testcase_results = {
        testcase_id: metadata['status'].lower()
        for testcase_id, metadata in testcases.items()
        if in_scope(scope, metadata, scopes_errors)
    }

    threshold = rule['threshold']
    if scopes_errors:
        return {
            'result': INVALID_SCOPE,
            'scope': scope,
            'threshold': threshold,
            'message': '\n'.join(scopes_errors),
        }
    if not testcase_results:
        return {'result': NOTEST, 'scope': scope, 'threshold': threshold}

    failure_status = rule.get('failure-status', DEFAULT_FAILURE_STATUS)
    failedtests_count = sum(
        1 for result in testcase_results.values() if result in failure_status
    )

    testcases_count = len(testcase_results)
    passed_count = testcases_count - failedtests_count
    return {
        'result': SUCCESS if _pass(threshold) else FAILURE,
        'scope': scope,
        'tests_in_scope': testcases_count,
        'tests_failed': failedtests_count,
        'tests_passed': passed_count,
        'success_ratio': f'{round(((passed_count/ testcases_count)*100), 2)}%',
        'threshold': threshold,
    }


def evaluate_quality_gate(
    mode: str, testcases: List[Dict[str, Any]], qualitygates: Dict[str, Any]
) -> Tuple[Dict[str, Any], Optional[List[str]]]:
    """Evaluate a quality gate.

    # Returned value

    A tuple.  The first entry is a dictionary, with one entry per rule.
    See #evaluate_rule() for details.  The second entry is either None
    or a list of strings, the warnings issued while evaluating the
    quality gate.

    # Raised exceptions

    A _ValueError_ exception is raised if the `mode` is unknown.
    """
    testcases_dict = {item['uuid']: item for item in testcases}
    if qualitygate := qualitygates.get(mode):
        return (
            {
                rule.get('name', make_uuid()): evaluate_rule(
                    rule['rule'], testcases_dict, mode
                )
                for rule in qualitygate
            },
            _check_duplicates(
                [rule['name'] for rule in qualitygate if rule.get('name')], 'rule'
            ),
        )

    raise ValueError(f'Quality gate {mode} not defined.')


########################################################################
# handlers


def _compute_qualitygate_general_status(rules_results):
    statuses = {value['result'] for value in rules_results.values()}
    if FAILURE in statuses or INVALID_SCOPE in statuses:
        return FAILURE
    return SUCCESS if SUCCESS in statuses else NOTEST


def handle_completed_workflow(
    workflow_id: str,
    response: requests.Response,
    mode: str,
    qualitygates: Dict[str, Any],
    warnings: Optional[List[str]] = None,
) -> Response:
    """Completed workflow handler."""
    observer = get_context_service(app, 'observer')
    url_base = f'{observer["endpoint"]}/workflows/{workflow_id}/datasources/testcases'
    try:
        testcases = response.json()['details']['items']
        while 'next' in response.links:
            url = f'{url_base}?{urlparse(response.links["next"]["url"]).query}'
            response = requests.get(url, headers=response.request.headers)
            testcases += response.json()['details']['items']
    except (ValueError, KeyError) as err:
        error('Could not deserialize observer response: %s.', str(err))
        debug(response.text)
        return make_status_response(
            'Invalid', f'Error while querying datasources: {str(err)}.'
        )
    rules_results, extra = evaluate_quality_gate(mode, testcases, qualitygates)

    for rule, result in rules_results.items():
        if result['result'] == INVALID_SCOPE:
            error(f"Error in rule {rule}. {result['message']}")

    general_status = _compute_qualitygate_general_status(rules_results)
    status = {'status': general_status, 'rules': rules_results}
    if warnings or extra:
        status['warnings'] = (warnings or []) + (extra or [])
        for msg in status['warnings']:
            warning(msg)
    return annotate_response(make_status_response('OK', '', status), processed=['mode'])


def handle_running_workflow(*_) -> Response:
    """Running workflow handler."""
    return make_status_response('OK', '', {'status': RUNNING})


# TODO: Should we now try to evaluate failed workflows or not?
def handle_failed_workflow(*_) -> Response:
    """Failed workflow handler."""
    return make_status_response('OK', '', {'status': FAILURE})


STATUS_HANDLER = {
    'DONE': handle_completed_workflow,
    'RUNNING': handle_running_workflow,
    'PENDING': handle_running_workflow,
    'FAILED': handle_failed_workflow,
}


def process_known_workflow(
    workflow_id: str,
    observer_response: requests.Response,
    mode: str,
    qualitygates: Dict[str, Any],
    warnings: Optional[List[str]] = None,
) -> Response:
    """Handling known workflow."""
    try:
        body = observer_response.json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if 'details' not in body or 'status' not in body['details']:
        return make_status_response(
            'BadRequest', 'Missing .details.status entry in body.'
        )

    if not body['details']['items'] and body['details']['status'] not in (
        'RUNNING',
        'PENDING',
    ):
        return make_status_response(
            'OK', 'No testcases found for the workflow.', {'status': NOTEST}
        )

    try:
        namespace = _get_namespace(body['details'].get('items', []))
    except ValueError:
        return handle_running_workflow()

    if not can_use_namespace(namespace, resource='qualitygates', verb='get'):
        return make_status_response(
            'Forbidden',
            f'Token not allowed to access workflows in namespace {namespace}.',
        )

    workflow_status = body['details']['status']
    if workflow_status not in STATUS_HANDLER:
        return make_status_response(
            'Invalid',
            f'Unexpected workflow status {workflow_status}, was expecting one of {",".join(STATUS_HANDLER)}.',
        )
    return STATUS_HANDLER[workflow_status](
        workflow_id, observer_response, mode, qualitygates, warnings
    )


# Observer datasources first page check


def find_datasource_items(workflow_id: str, token: Optional[str]) -> requests.Response:
    """Read first page of workflow datasource items."""
    observer = get_context_service(app, 'observer')
    url = f'{observer["endpoint"]}/workflows/{workflow_id}/datasources/testcases'
    headers = {'Authorization': token} if token else None
    return requests.get(url, headers=headers)


########################################################################
# Main

app = make_app(
    name='qualitygate',
    description='Create and start a quality gate service.',
    schema=SERVICECONFIG,
)


def _get_user_qualitygate_definition():
    """Get quality gate definition from request."""
    if request.is_json:
        try:
            return request.get_json() or {}
        except Exception as err:
            error(f'Could not parse quality gate definition: {err}')

    if request.mimetype in ('application/x-yaml', 'text/yaml'):
        if request.content_length and request.content_length < 1000000:
            return yaml.safe_load(request.data)
        raise Exception(
            'Content-size must be specified and less than 1000000 for YAML quality gate definitions.'
        )
    if 'qualitygates' in request.files:
        return yaml.safe_load(request.files['qualitygates'].read())
    raise Exception('Could not parse quality gate definition from file.')


def _maybe_evaluate_qualitygate(
    workflow_id: str, qualitygates: Dict[str, Any], warnings: Optional[List[str]]
) -> Response:
    """Evaluate qualitygate if the incoming request is valid.

    # Returned value

    A Response object.
    """
    mode = request.args.get('mode', 'strict')
    if mode not in qualitygates:
        return make_status_response(
            'Invalid',
            f'Quality gate {mode} not found in definition file.',
        )
    if mode in ['strict', 'passing']:
        warnings = None
    if not is_uuid(workflow_id):
        return make_status_response(
            'Invalid', f'This parameter is not a valid UUID: {workflow_id}'
        )

    token = request.headers.get('Authorization')
    response = find_datasource_items(workflow_id, token)
    status_code = response.status_code
    debug('Got observer response %d for workflow %s.', status_code, workflow_id)
    if status_code == 200:
        return process_known_workflow(
            workflow_id, response, mode, qualitygates, warnings
        )
    if status_code == 401:
        return make_status_response('Unauthorized', 'Invalid or missing token.')
    if status_code == 403:
        return make_status_response(
            'Forbidden', f'Token does not allow access to workflow {workflow_id}.'
        )
    if status_code == 404:
        return make_status_response('NotFound', f'Workflow {workflow_id} not found.')
    if status_code == 422:
        return make_status_response(
            'Invalid',
            f'Invalid datasource kind for workflow {workflow_id}, should not happen.',
        )

    error('Unexpected observer response: %d.', status_code)
    return make_status_response(
        'InternalError', f'Unexpected observer response {status_code}.'
    )


@app.route('/workflows/<workflow_id>/qualitygate', methods=['POST'])
@authorizer(resource='qualitygates', verb='get')
def handle_qualitygate_definition(workflow_id: str):
    """Handle quality gate definition from request."""
    try:
        qualitygate_definition = _get_user_qualitygate_definition()
        valid, extra = validate_schema(QUALITY_GATE, qualitygate_definition)
        if not valid:
            raise Exception(f'Not a valid quality gate definition: {extra}')
        warnings = _check_duplicates(
            [
                qualitygate['name']
                for qualitygate in qualitygate_definition['qualitygates']
            ],
            'quality gate',
        )
        qualitygates = {
            qualitygate['name']: qualitygate['rules']
            for qualitygate in qualitygate_definition['qualitygates']
        }
    except Exception as err:
        return make_status_response(
            'Invalid', str(err), details={'workflow_id': workflow_id}
        )
    return _maybe_evaluate_qualitygate(workflow_id, qualitygates, warnings)


@app.route('/workflows/<workflow_id>/qualitygate', methods=['GET'])
@authorizer(resource='qualitygates', verb='get')
def get_workflow_status(workflow_id: str):
    """
    Main method of the quality gate service. Will try to pull the testcases
    datasource from observer for a given workflow and will analyze the response
    to send back a status to caller.

    - If the observer responds with an error, quality gate will respond
      with HTTP error codes.
    - If the observer responds HTTP 200, the quality gate will try to
      analyze tests results and provide a status.

    Given an HTTP 200 response from observer:

    - If the workflow has not a SUCCESS status, quality gate will return
      HTTP 200 + custom status in details.status.
    - If the workflow has SUCCESS status (aka is completed), quality
      gate will use a named strategy to analyze tests results, and
      return HTTP 200 + custom status in details.status attribute.
    """
    return _maybe_evaluate_qualitygate(
        workflow_id,
        app.config['CONFIG']['qualitygates'],
        app.config['CONFIG'].get('qualitygate_warnings'),
    )


def _load_default_qg_definitions():
    """Load default quality gate definitions (strict and passing)."""
    qualitygates = app.config['CONFIG'].setdefault('qualitygates', {})
    qualitygates['strict'] = DEFAULT_STRICT_QUALITY_GATE
    qualitygates['passing'] = DEFAULT_PASSING_QUALITY_GATE
    info(
        'Loading default quality gate definitions.  Use `-m strict` or `-m passing` to apply.'
    )


def _read_qg_definition(_, configfile: Optional[str], schema: str):
    """Read quality gate definition from environment variable specified file."""
    try:
        if not configfile:
            return

        if old := 'qualitygates' in app.config['CONFIG']:
            del app.config['CONFIG']['qualitygates']
        with open(configfile, 'r', encoding='utf-8') as src:
            qualitygates = yaml.safe_load(src)
        if not isinstance(qualitygates, dict) or 'qualitygates' not in qualitygates:
            error(
                f'Invalid quality gate definition file {configfile} structure or "qualitygates" entry missing, ignoring.'
            )
            return
        valid, extra = validate_schema(schema, qualitygates)
        if not valid:
            error(
                f'Error while verifying quality gate definition {configfile}: {extra}.'
            )
            return

        if old:
            info(f'Replacing quality gate definition using {configfile}.')
        else:
            info(f'Reading quality gate definition from {configfile}.')

        warnings = _check_duplicates(
            [qualitygate['name'] for qualitygate in qualitygates['qualitygates']],
            'quality gate',
        )
        if warnings:
            for msg in warnings:
                warning(msg)
        app.config['CONFIG']['qualitygate_warnings'] = warnings
        app.config['CONFIG']['qualitygates'] = {
            qualitygate['name']: qualitygate['rules']
            for qualitygate in qualitygates['qualitygates']
        }

    except Exception as err:
        error(f'Error while reading {configfile} quality gate definition: {err}.')
    finally:
        _load_default_qg_definitions()


def main(svc):
    """Run the quality gate service."""
    _ = get_context_service(svc, 'observer')
    if os.environ.get('QUALITYGATE_DEFINITIONS'):
        watch_file(
            svc,
            os.environ['QUALITYGATE_DEFINITIONS'],
            _read_qg_definition,
            QUALITY_GATE,
        )
    else:
        svc.logger.info('No qualitygate definition file provided.')
        _read_qg_definition(svc, None, QUALITY_GATE)
    run_app(svc)


if __name__ == '__main__':
    main(app)
